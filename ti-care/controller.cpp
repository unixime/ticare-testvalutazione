#include <QSet>
#include <QDateTime>
#include "tasks.h"

/**
 * @brief isElegibleToStart
 * This function decides if a task has to execute based on its
 * scheduling setting. The decision is taken by comparison
 * between scheduling setting and the current time
 *
 * @param[in] ts the task scheduling setting
 *
 * @retval  true    The task has to start
 * @retval  false   the task has not to start
 */
bool isElegibleToStart(const task::TaskSched& ts)
{

    QDateTime now = QDateTime::currentDateTime();

    qint16 mins = now.time().minute();
    qint16 hours = now.time().hour();
    qint16 days = now.date().day();
    qint16 months = now.date().month();
    qint16 dows = now.date().dayOfWeek();

    if ( ts.minutes.size() != 1 && ts.minutes.values()[0] != -1 )
    {
        if ( !ts.minutes.contains(mins))
            return false;
    }

    if ( ts.hours.size() != 1 && ts.hours.values()[0] != -1 )
    {
        if ( !ts.hours.contains(hours))
            return false;
    }

    auto allDaysSchedule = ((ts.days.size() == 1) && (ts.days.values()[0] == -1));
    auto allDowsSchedule = ((ts.dows.size() == 1) && (ts.dows.values()[0] == -1));

    if ( !allDaysSchedule || !allDowsSchedule)
    {

        if ( !ts.days.contains(days) && !ts.dows.contains(dows) )
        {
            return false;
        }
    }

    if ( ts.months.size() != 1 && ts.months.values()[0] != -1 )
    {
        if ( !ts.months.contains(months) && !ts.months.contains(months) )
        {
            return false;
        }
    }

    return true;
}

