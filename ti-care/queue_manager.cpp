
#include <QDebug>
#include <QDateTime>
#include <QThread>
#include <QTimer>

#include "queue_manager.h"

QueueManager::QueueManager(QObject *parent):QThread (parent)
{
    this->queueClock = new QTimer();
    this->queueClock->start(5000);
    connect(this->queueClock, SIGNAL(timeout()), this, SLOT(process()));
}

QueueManager::~QueueManager()
{
    if (this->queueClock)
        delete this->queueClock;
}



/**
 * @brief QueueManager::add_queue
 * Adds a task to queue of process to execute
 * @param[in]   t   the task to add
 */
void QueueManager::add_queue(task::TaskBase* t)
{
    this->queue_in_mutex.lock();
    this->q.append(t);
    this->queue_in_mutex.unlock();
}

/**
 * @brief QueueManager::process
 *
 * Tasks' queue manager: every SECS the queue is checked
 * and if it is not empty all tasks it contains will be executed.
 * The tasks will be executed in the same order
 * they have been added to queue (FIFO).
 */
void QueueManager::process()
{
    if(this->wall)
    {
        if(!this->q.empty())
        {
            this->queue_out_mutex.lock();
            task::TaskBase* s = this->q.dequeue();
            this->queue_out_mutex.unlock();

            s->execute();

            //qDebug() << QDateTime::currentDateTime() << "  | Task " << s->tag << " completed.";

        }
        else
        {
            qDebug() << QDateTime::currentDateTime().toString() << "Nothing to do ...";
        }
    }
}

/**
 * @brief QueueManager::run
 *
 * Starts the thread used to manage the queue.
 */
void QueueManager::run()
{
    this->wall=true;
}
