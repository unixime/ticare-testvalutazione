#include "tasks.h"

#include <iostream>
#include <QDebug>
#include <QFile>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

namespace task
{
    TaskBase::TaskBase()
    {
        clock = new QTimer();
    }

    TaskBase::~TaskBase()
    {
        if(clock)
            delete clock;
    }

    bool TaskBase::SetSchedule(const QString& s)
    {
        theSched.reset();

        auto comps = s.split(" ");

        if (comps.length() != 5 && comps.length() != 1)
            return false;

        if (comps.length() == 1)
        {
            this->et = execType::PERIODIC;
            return true;
        }
        else
        {
            this->et = execType::SCHED;
        }

        QStringList nitems;

        nitems = comps[0].split(",");
        if ( nitems.length() == 1 && comps[0]=="*" )
            theSched.minutes.insert(-1);
        else
        {
            for ( auto item: comps[0].split(",") )
                theSched.minutes.insert( item.toInt() );
        }


        nitems = comps[1].split(",");
        if ( nitems.length() == 1 && comps[1]=="*" )
            theSched.hours.insert(-1);
        else
        {
            for ( auto item: comps[1].split(",") )
                theSched.hours.insert( item.toInt() );
        }


        nitems = comps[2].split(",");
        if ( nitems.length() == 1 && comps[2]=="*" )
            theSched.days.insert(-1);
        else
        {
            for ( auto item: comps[2].split(",") )
                theSched.days.insert( item.toInt() );
        }


        nitems = comps[3].split(",");
        if ( nitems.length() == 1 && comps[3]=="*" )
            theSched.months.insert(-1);
        else
        {
            for ( auto item: comps[3].split(",") )
                theSched.months.insert( item.toInt() );
        }


        nitems = comps[4].split(",");
        if ( nitems.length() == 1 && comps[4]=="*" )
            theSched.dows.insert(-1);
        else
        {
            for ( auto item: comps[4].split(",") )
                theSched.dows.insert( item.toInt() );
        }

        return true;

    }

    const TaskSched& TaskBase::GetSchedule() const
    {
        return this->theSched;
    }

    bool TaskBase::isPeriodicTask() const
    {
        return et == execType::PERIODIC;
    }

    bool TaskBase::isSceduledTask() const
    {
        return et == execType::SCHED;
    }

    void TaskBase::Start(int timeout)
    {
        if ( this->clock->isActive() )
            this->clock->stop();

        this->clock->start(timeout);
    }

    /**
     * @brief TaskOne::TaskOne
     *
     * Task 1 C-Tor
     */
    TaskOne::TaskOne()
    {
        this->tag = "Task 1";
        connect(this->clock, SIGNAL(timeout()), this, SLOT(execute()));
    }

    bool TaskOne::execute()
    {
        qDebug() << QDateTime::currentDateTime().toString() << " | Task1 | " << this->msg;
        return true;
    }


    /**
     * @brief TaskTwo::TaskTwo
     *
     * Task 2 C-Tor
     */
    TaskTwo::TaskTwo()
    {
        this->tag = "Task 2";
        connect(this->clock, SIGNAL(timeout()), this, SLOT(execute()));
    }

    bool TaskTwo::execute()
    {
        QFile f2look(this->filename);

        if ( f2look.exists() )
            qDebug() << QDateTime::currentDateTime().toString() << " | Task2 | " << "file " << this->filename << " exists";
        else
            qDebug() << QDateTime::currentDateTime().toString() << " | Task2 | " << "file " << this->filename << " doesn't exist";

        return true;
    }

}
