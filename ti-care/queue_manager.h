#ifndef QUEUE_MANAGER_H
#define QUEUE_MANAGER_H


#include <QThread>
#include <QMutex>
#include <QQueue>

#include "tasks.h"

class QTimer;

class QueueManager : public QThread
{
    Q_OBJECT

public:
    QueueManager(QObject *parent = nullptr);
    ~QueueManager();

    inline void stop(){this->wall=false;}

public slots:
    void add_queue(task::TaskBase*);
    void process();

private:
    bool    wall{false};

    QTimer* queueClock{nullptr};

    QMutex  queue_in_mutex;
    QMutex  queue_out_mutex;
    QQueue<task::TaskBase*> q;

    void run();

};

#endif // QUEUE_MANAGER_H
