#ifndef TASKS_H
#define TASKS_H

#include <QTimer>
#include <QString>
#include <QDateTime>
#include <QSet>
#include <QDebug>

namespace task
{
    struct TaskSched
    {
        //QSet<qint16> minutes{0, 15, 20};
        QSet<qint16> minutes{-1};
        QSet<qint16> hours{-1};
        QSet<qint16> days{-1};
        QSet<qint16> months{-1};
        QSet<qint16> dows{-1};

        void reset()
        {
            this->minutes.clear();
            this->hours.clear();
            this->days.clear();
            this->months.clear();
            this->dows.clear();
        }
    };

    enum execType
    {
        PERIODIC = 1,
        SCHED = 2,
        __latest__
    };

    class TaskBase : public QObject
    {

    public:
        TaskBase();
        virtual ~TaskBase();
        bool virtual execute() = 0;

        bool SetSchedule(const QString& s);
        const TaskSched& GetSchedule() const;

        bool isPeriodicTask() const;
        bool isSceduledTask() const;

        void Start(int timeout);
        void Stop(){this->clock->stop();}

        QString tag;
        TaskSched   theSched;
        bool enabled{false};

        execType et{PERIODIC};

        QTimer* clock{nullptr};

    };


    class TaskOne : public TaskBase
    {

        Q_OBJECT

    public:
        TaskOne();
        inline void Msg(QString& msg){this->msg = msg;}
        inline const  QString& Msg() const {return this->msg;}

    public slots:
        bool execute();

    private:
        QString msg;
    };

    class TaskTwo : public TaskBase
    {
        Q_OBJECT

    public:
        TaskTwo();
        inline void FileName(QString& fname){this->filename = fname;}
        inline const QString& FileName() const {return this->filename;}

    public slots:
        bool execute();

    private:
        QString filename;
    };

}

#endif // TASKS_H
