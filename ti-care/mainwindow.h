#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "tasks.h"

class QTabWidget;
class QLineEdit;
class QPushButton;
class QueueManager;
class QTimer;
class QSystemTrayIcon;
class QRadioButton;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    static const int APP_CLOCK{60000};

    QTimer*         tasksCheckTimer{nullptr};
    QSystemTrayIcon *sys_try{nullptr};

    QTabWidget*     tabs{nullptr};

    QLineEdit*      task1Sched{nullptr};
    QLineEdit*      task1Msg{nullptr};
    QPushButton*    task1StartButton{nullptr};
    QPushButton*    task1StopButton{nullptr};
    QRadioButton*   task1CronAction{nullptr};
    QRadioButton*   task1IntervalAction{nullptr};

    QLineEdit*      task2Sched{nullptr};
    QLineEdit*      task2File{nullptr};
    QPushButton*    task2StartButton{nullptr};
    QPushButton*    task2StopButton{nullptr};
    QRadioButton*   task2CronAction{nullptr};
    QRadioButton*   task2IntervalAction{nullptr};

    QueueManager*   theTaskQueue{nullptr};

    task::TaskOne* task1{nullptr};
    task::TaskTwo* task2{nullptr};

    void createTasksTabs();
    void defineTasks();

    void createTask1SettingTab();
    void connectTask1Buttons();

    void createTask2SettingTab();
    void connectTask2Buttons();

    void connectTimer();

    //void manageTask(task::TaskBase*);

    void initTimer();

    void setWinSize();

    void createSysTry();

private slots:
    void manageTask1Start();
    void manageTask1Stop();

    void manageTask2Start();
    void manageTask2Stop();

    void manageTasks();

    void resizeMainWindow();

};

#endif // MAINWINDOW_H
