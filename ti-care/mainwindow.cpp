#include <QTabWidget>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QDebug>
#include <QSystemTrayIcon>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include <QDateTime>
#include <QRadioButton>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "queue_manager.h"

bool isElegibleToStart(const task::TaskSched& ts);

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    this->setWindowTitle("Ti-Care Verification Test");


    this->initTimer();
    this->createSysTry();
    this->createTasksTabs();

    this->setWinSize();

}

MainWindow::~MainWindow()
{
    this->tasksCheckTimer->stop();
    this->theTaskQueue->stop();
    while(!theTaskQueue->isFinished())
        QThread::sleep(1);

    delete ui;
}

void MainWindow::setWinSize()
{
    this->setMaximumSize(400, 300);
}


void MainWindow::resizeMainWindow()
{
    this->resize(400, 300);
}


void MainWindow::createTasksTabs()
{
    tabs = new QTabWidget(this);


    this->createTask1SettingTab();
    this->connectTask1Buttons();

    this->createTask2SettingTab();
    this->connectTask2Buttons();

    this->defineTasks();

    theTaskQueue = new QueueManager(this);
    theTaskQueue->start();


    this->setCentralWidget(tabs);
}

void MainWindow::defineTasks()
{
    this->task1 = new task::TaskOne();
    this->task2 = new task::TaskTwo();


    this->connectTimer();
    //this->initTimer();
}


/**
 * @brief MainWindow::createTask1SettingTab
 *
 * Defines the Tab to manage the embedded task1
 */
void MainWindow::createTask1SettingTab()
{
    QWidget* tabTask1 = new QWidget(tabs);

    QHBoxLayout* tabTask1SchedLyt = new QHBoxLayout();
    tabTask1SchedLyt->addWidget(new QLabel("Schedule"));
    task1Sched = new QLineEdit("10000");
    tabTask1SchedLyt->addWidget(task1Sched);


    QHBoxLayout* tabTask1MessageLyt = new QHBoxLayout();
    tabTask1MessageLyt->addWidget(new QLabel("The Message"));
    task1Msg = new QLineEdit("the message");
    tabTask1MessageLyt->addWidget(task1Msg);

    QHBoxLayout* tabTask1ButtonsLyt = new QHBoxLayout();
    task1StartButton = new QPushButton("Start");
    task1StartButton->setEnabled(true);
    tabTask1ButtonsLyt->addWidget(task1StartButton);
    task1StopButton = new QPushButton("Stop");
    task1StopButton->setEnabled(false);
    tabTask1ButtonsLyt->addWidget(task1StopButton);

    QHBoxLayout *chkbox = new QHBoxLayout;
    task1CronAction = new QRadioButton(tr("Cron"));
    task1CronAction->setChecked(false);
    task1IntervalAction = new QRadioButton(tr("Periodic"));
    task1IntervalAction->setChecked(true);
    chkbox->addWidget(task1CronAction);
    chkbox->addWidget(task1IntervalAction);


    QVBoxLayout* tabTask1VLyt = new QVBoxLayout();
    tabTask1VLyt->addLayout(chkbox);
    tabTask1VLyt->addLayout(tabTask1SchedLyt);
    tabTask1VLyt->addLayout(tabTask1MessageLyt);
    tabTask1VLyt->addLayout(tabTask1ButtonsLyt);

    tabTask1->setLayout(tabTask1VLyt);

    tabs->addTab(tabTask1, "Task1 control");
}


/**
 * @brief MainWindow::manageTask1Start
 *
 * Collects the Task1 setting from GUI controls and
 * enables the embedded Task1
 */
void MainWindow::manageTask1Start()
{

    this->task1->enabled = true;

    auto msg = task1Msg->text();
    this->task1->Msg(msg);

    auto txt = task1Sched->text();
    this->task1->SetSchedule(txt);

    if ( this->task1->isPeriodicTask() )
        this->task1->Start(txt.toInt());

    task1StartButton->setEnabled(false);
    task1StopButton->setEnabled(true);

}


/**
 * @brief MainWindow::manageTask1Stop
 *
 * Stops the Task1
 */
void MainWindow::manageTask1Stop()
{

    this->task1->enabled = false;

    if ( this->task1->isPeriodicTask())
    {
        this->task1->Stop();
    }

    task1StopButton->setEnabled(false);
    task1StartButton->setEnabled(true);

    this->task1->enabled = false;

    qDebug() << QDateTime::currentDateTime().time() << " Task 1 stopped.";



}

/**
 * @brief MainWindow::connectTask1Buttons
 *
 * Connects the Start e Stop QPushButton that control the embedded Task1
 */
void MainWindow::connectTask1Buttons()
{
    connect(task1StartButton, SIGNAL(clicked()), this, SLOT(manageTask1Start()));
    connect(task1StopButton, SIGNAL(clicked()), this, SLOT(manageTask1Stop()));
}


/**
 * @brief MainWindow::createTask2SettingTab
 *
 * Defines the Tab to manage the embedded task1
 */
void MainWindow::createTask2SettingTab()
{
    QWidget* tabTask2 = new QWidget(tabs);

    QHBoxLayout* tabTask2SchedLyt = new QHBoxLayout();
    tabTask2SchedLyt->addWidget(new QLabel("Schedule"));
    task2Sched = new QLineEdit("10000");
    tabTask2SchedLyt->addWidget(task2Sched);


    QHBoxLayout* tabTask2MessageLyt = new QHBoxLayout();
    tabTask2MessageLyt->addWidget(new QLabel(tr("Look for :")));
    task2File = new QLineEdit();
    tabTask2MessageLyt->addWidget(task2File);

    QHBoxLayout* tabTask2ButtonsLyt = new QHBoxLayout();
    task2StartButton = new QPushButton("Start");
    task2StartButton->setEnabled(true);
    tabTask2ButtonsLyt->addWidget(task2StartButton);
    task2StopButton = new QPushButton("Stop");
    task2StopButton->setEnabled(false);
    tabTask2ButtonsLyt->addWidget(task2StopButton);

    QHBoxLayout *chkbox = new QHBoxLayout;
    task2CronAction = new QRadioButton(tr("Cron"));
    task2CronAction->setChecked(false);
    task2IntervalAction = new QRadioButton(tr("Periodic"));
    task2IntervalAction->setChecked(true);
    chkbox->addWidget(task2CronAction);
    chkbox->addWidget(task2IntervalAction);

    QVBoxLayout* tabTask2VLyt = new QVBoxLayout();
    tabTask2VLyt->addLayout(chkbox);
    tabTask2VLyt->addLayout(tabTask2SchedLyt);
    tabTask2VLyt->addLayout(tabTask2MessageLyt);
    tabTask2VLyt->addLayout(tabTask2ButtonsLyt);

    tabTask2->setLayout(tabTask2VLyt);

    tabs->addTab(tabTask2, "Task2 control");
}


/**
 * @brief MainWindow::manageTask1Start
 *
 * Collects the Task1 setting from GUI controls and
 * enables the embedded Task2
 */
void MainWindow::manageTask2Start()
{

    this->task2->enabled = true;

    auto fn = task2File->text();
    this->task2->FileName(fn);

    auto txt = task2Sched->text();
    this->task2->SetSchedule(txt);

    if ( this->task2->isPeriodicTask() )
        this->task2->Start(txt.toInt());

    task2StartButton->setEnabled(false);
    task2StopButton->setEnabled(true);

}


/**
 * @brief MainWindow::manageTask2Stop
 *
 * Stops the embedded task Task2
 */
void MainWindow::manageTask2Stop()
{

    this->task2->enabled = false;

    if ( this->task2->isPeriodicTask())
    {
        this->task2->Stop();
    }

    task2StopButton->setEnabled(false);
    task2StartButton->setEnabled(true);

    qDebug() << QDateTime::currentDateTime().time() << " Task 2 stopped.";

}


/**
 * @brief MainWindow::connectTask2Buttons
 *
 * Connects the Start e Stop QPushButton that control the embedded Task2
 */
void MainWindow::connectTask2Buttons()
{
    connect(task2StartButton, SIGNAL(clicked()), this, SLOT(manageTask2Start()));
    connect(task2StopButton, SIGNAL(clicked()), this, SLOT(manageTask2Stop()));
}

/**
 * @brief MainWindow::manageTasks
 *
 * Manages the tasks configured to start in according to a Cron policy.
 *
 * For both embedded tasks, this method checks if the single task is enabled
 * and if the current date matches with the task's Cron setting. If so, the task
 * is put into the queue ( QueueManager ) of tasks ready to start.
 */
void MainWindow::manageTasks()
{
    if ( this->task1->enabled && this->task1->isSceduledTask())
    {
        if ( isElegibleToStart( this->task1->GetSchedule() ) )
            this->theTaskQueue->add_queue(task1);
    }

    if ( this->task2->enabled && this->task2->isSceduledTask())
    {
        if ( isElegibleToStart( this->task2->GetSchedule() ) )
            this->theTaskQueue->add_queue(task2);
    }

}


/**
 * @brief MainWindow::manageTask
 * @param[in] theTask   the task reference of task handle
 *
 * If the task is not a periodic task, this method checks
 * if the task schedule matches the Cron setting.
 */
/*
void MainWindow::manageTask(task::TaskBase* theTask)
{

    if ( theTask->isPeriodicTask() )
        return;

    const task::TaskSched t = theTask->GetSchedule();

    if ( theTask->enabled && isElegibleToStart(t) )
        this->theTaskQueue->add_queue(theTask);
}
*/


/**
 * @brief MainWindow::initTimer
 *
 * Initializes the Apllication times (QTimer)
 */
void MainWindow::initTimer()
{
    /*
    auto rouded_date = QDateTime(QDate(QDateTime::currentDateTime().date().year(),
                                 QDateTime::currentDateTime().date().month(),
                                 QDateTime::currentDateTime().date().day()),
                                 QTime(QDateTime::currentDateTime().time().hour(),
                                 QDateTime::currentDateTime().time().minute(),
                                 0));

    auto now = QDateTime::currentDateTime();
    auto next_min = rouded_date.addSecs(60);
    auto delay = now.msecsTo(next_min);

    QThread::msleep(delay);
    */

    this->tasksCheckTimer = new QTimer(this);

    this->tasksCheckTimer->setInterval(APP_CLOCK);
    this->tasksCheckTimer->start();
    this->statusBar()->showMessage(QDateTime::currentDateTime().toString() + " - App timer started.");
}

/**
 * @brief MainWindow::connectTimer
 *
 * Connets the timeout slot coming from tasksCheckTimer QTimer to the slot
 * that handles the two embedded tasks
 */
void MainWindow::connectTimer()
{
    connect(this->tasksCheckTimer, SIGNAL(timeout()), this, SLOT(manageTasks()));
}


void MainWindow::createSysTry()
{
    if (!QSystemTrayIcon::isSystemTrayAvailable())
        return;


    QAction *minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, &QAction::triggered, this, &QWidget::hide);

    /*
    QAction *maximizeAction = new QAction(tr("Ma&ximize"), this);
    connect(maximizeAction, &QAction::triggered, this, &QWidget::showMaximized);
    */

    QAction *restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, &QAction::triggered, this, &QWidget::showNormal);

    QAction *quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);

    QMenu *sys_try_menu = new QMenu(this);

    sys_try_menu->addAction(minimizeAction);
    //sys_try_menu->addAction(maximizeAction);
    sys_try_menu->addAction(restoreAction);
    sys_try_menu->addSeparator();
    sys_try_menu->addAction(quitAction);


    sys_try = new QSystemTrayIcon(this);
    sys_try->setContextMenu(sys_try_menu);
    sys_try->setIcon(QIcon(":/images/res/cowboy_hat.png"));
    sys_try->show();

}
