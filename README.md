# Application description ##

Questa applicazione permette di gestire l'esecuzione dei due seguenti task predefiniti:

* Task1: stampare sullo stream di output ( qDebug ) un messaggio configurabile dall'utente.

* Task2 : verificare l'esistenza di un file scelto dall'utente.

Per entrambe i task, l'utente potrà definire la modalità di esecuzione dei task ( Cron o periodici ).



## Environment di sviluppo/test ##

 * OS : Debian 9.5
 * QTCreator : 4.7.1
 * QT : 5.11

## Limiti dell'applicazione ##
L'applicazione ha i seguiti limiti:
* non può definire nuovi task a run-time
* applicazione non esegue alcun controllo sulla stringa di schedulazine dei task: una volta scelto la tipologia di esecuzione ( Cron o Periodica ), l'utente deve riempire correttamente la stringa di schedulazione
    
    * un singolo valore ( msec ) nel caso di tesk periodici
    * 5 valori come da definizione di un processo Cron ( https://en.wikipedia.org/wiki/Cron )
* applicazione non esegue alcun controllo sulla stringa di schedulazine dei task. Per fixare questo punto sarebbe utile definire una QRegExp che validi il formato della stringa di schedulazione (https://en.wikipedia.org/wiki/Cron). Una volta validato il formato andrebbero validati i 5 campi della stringa quando questi non contengono il carattere '*' secondo le specifiche descritte nel link di cui sopra.


## Task Temporizzati ##

Per entrambe i task predefiniti, l'utente puòla periodicità di esecuzione in msecs (millisecondi);

![Image of Timed Task](./docimg/TimedTask.png?raw=true)

## Schedulazione dei task ##

Per entrambe i task predefiniti, l'utente può schedularne l'esecuzione usando il formato tipico del processo Cron (https://en.wikipedia.org/wiki/Cron)

Per maggiori dettagli in merito all'embedded scheduler si faccia riferiemnto al file  **isElegibleToStart.jpeg**.

![Image of Cron Tasks](./docimg/CronTask.png?raw=true)

Esempio 1: Lo specifico task sarà eseguito ogni minuto

```
* * * * *
```

Esempio 2: Lo specifico task sarà eseguito ai minuti 00,10,20,30,40 e 50 di ogni ora
```
0,10,20,30,40,50 * * * *
```

Esempio 3: Lo specifico task sarà eseguito ogni giorno alle 12:15
```
15 12 * * *
```

Esempio 3: Lo specifico task sarà eseguito ogni giorno alle 12:15
```
15 12 * * 5
```
The task will be executed every Friday at 12:15

```
15 12 10,20,30 3 5
```
The task will be executed every Friday or 10th, 20th, 30th day in March at 12:15